module Data.Alignment exposing (Alignment(..), LawAlignment(..), MoralAlignment(..), decoder, toJson)

import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode


type LawAlignment
    = Lawful
    | LawNeutral
    | Chaotic


type MoralAlignment
    = Good
    | MoralNeutral
    | Evil


type Alignment
    = Unaligned
    | Aligned ( LawAlignment, MoralAlignment )



-- ENCODE


toJson : Alignment -> Encode.Value
toJson alignment =
    case alignment of
        Unaligned ->
            Encode.string "UNALIGNED"

        Aligned ( lawAlignment, moralAlignment ) ->
            Encode.object
                [ ( "lawAlignment", encodeLawAlignment lawAlignment )
                , ( "moralAlignment", encodeMoralAlignment moralAlignment )
                ]


encodeLawAlignment : LawAlignment -> Encode.Value
encodeLawAlignment alignment =
    Encode.string <|
        case alignment of
            Lawful ->
                "LAWFUL"

            LawNeutral ->
                "NEUTRAL"

            Chaotic ->
                "CHAOTIC"


encodeMoralAlignment : MoralAlignment -> Encode.Value
encodeMoralAlignment alignment =
    Encode.string <|
        case alignment of
            Good ->
                "GOOD"

            MoralNeutral ->
                "NEUTRAL"

            Evil ->
                "EVIL"



-- DECODE


decoder : Decoder Alignment
decoder =
    Decode.oneOf [ unalignedDecoder, alignedDecoder ]


unalignedDecoder : Decoder Alignment
unalignedDecoder =
    Decode.string
        |> Decode.andThen
            (\str ->
                case str of
                    "UNALIGNED" ->
                        Decode.succeed Unaligned

                    _ ->
                        Decode.fail ("Did not recognize alignment string: " ++ str)
            )


alignedDecoder : Decoder Alignment
alignedDecoder =
    Decode.map2
        (\law moral -> Aligned ( law, moral ))
        (Decode.field "lawAlignment" lawDecoder)
        (Decode.field "moralAlignment" moralDecoder)


lawDecoder : Decoder LawAlignment
lawDecoder =
    Decode.string
        |> Decode.andThen
            (\str ->
                case str of
                    "LAWFUL" ->
                        Decode.succeed Lawful

                    "NEUTRAL" ->
                        Decode.succeed LawNeutral

                    "CHAOTIC" ->
                        Decode.succeed Chaotic

                    _ ->
                        Decode.fail ("Did not recognize law alignment string: " ++ str)
            )


moralDecoder : Decoder MoralAlignment
moralDecoder =
    Decode.string
        |> Decode.andThen
            (\str ->
                case str of
                    "GOOD" ->
                        Decode.succeed Good

                    "NEUTRAL" ->
                        Decode.succeed MoralNeutral

                    "EVIL" ->
                        Decode.succeed Evil

                    _ ->
                        Decode.fail ("Did not recognize moral alignment string: " ++ str)
            )
