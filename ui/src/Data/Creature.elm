module Data.Creature exposing (Action, Creature, CreatureType(..), MovementType(..), Sense(..), Size(..), decoder, encode)

import Data.Abilities as Abilities exposing (AbilityScores)
import Data.Alignment as Alignment exposing (Alignment)
import Data.Attack as Attack exposing (Attack, Damage, Dice)
import Data.Helpers exposing (customTypeDecoder, customTypeEncoder, union1Decoder)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Enc


type Size
    = Tiny
    | Small
    | Medium
    | Large
    | Huge
    | Gargantuan


type CreatureType
    = Abberation
    | Beast
    | Celestial
    | Construct
    | Dragon
    | Elemental
    | Fey
    | Fiend
    | Giant
    | Humanoid
    | Monstrosity
    | Ooze
    | Plant
    | Undead


type MovementType
    = Walk Int
    | Burrow Int
    | Climb Int
    | Fly Int
    | Swim Int


type Sense
    = Blindsight Int
    | Darkvision Int
    | Tremorsense
    | Truesight



{-
   , skills
   , langauges
   , challenge rating
   , multiattack
   , spells
   , reactions
-}


type alias Action =
    { name : String
    , charges : Maybe Int
    , description : String
    }


type alias Creature =
    { id : String
    , name : String
    , size : Size
    , creatureType : CreatureType
    , typeTags : List String
    , alignment : Alignment
    , ac : Int
    , movement : List MovementType
    , hp : Int
    , abilityScores : AbilityScores

    -- , senses : List Sense
    , xp : Int

    --    , attacks : List Attack
    --    , actions : List Action
    }



-- Encode / Decode


encode : Creature -> Enc.Value
encode creature =
    Enc.object
        [ ( "id", Enc.string creature.id )
        , ( "name", Enc.string creature.name )
        , ( "size", encodeSize creature.size )
        , ( "type", encodeType creature.creatureType )
        , ( "typeTags", Enc.list Enc.string creature.typeTags )
        , ( "alignment", Alignment.toJson creature.alignment )
        , ( "ac", Enc.int creature.ac )
        , ( "movement", Enc.list encodeMovement creature.movement )
        , ( "hp", Enc.int creature.hp )
        , ( "abilityScores", Abilities.toJson creature.abilityScores )

        --  , ( "senses", Enc.list encodeSense creature.senses )
        , ( "xp", Enc.int creature.xp )

        --        , ( "attacks", Enc.list Attack.toJson creature.attacks )
        --        , ( "actions", Enc.list encodeAction creature.actions )
        ]


sizeMapping =
    [ ( "TINY", Tiny )
    , ( "SMALL", Small )
    , ( "MEDIUM", Medium )
    , ( "LARGE", Large )
    , ( "HUGE", Huge )
    , ( "GARGANTUAN", Gargantuan )
    ]


encodeSize =
    customTypeEncoder sizeMapping


sizeDecoder =
    customTypeDecoder sizeMapping


typeMapping =
    [ ( "ABBERATION", Abberation )
    , ( "BEAST", Beast )
    , ( "CELESTIAL", Celestial )
    , ( "CONSTRUCT", Construct )
    , ( "DRAGON", Dragon )
    , ( "ELEMENTAL", Elemental )
    , ( "FEY", Fey )
    , ( "FIEND", Fiend )
    , ( "GIANT", Giant )
    , ( "HUMANOID", Humanoid )
    , ( "MONSTROSITY", Monstrosity )
    , ( "OOZE", Ooze )
    , ( "PLANT", Plant )
    , ( "UNDEAD", Undead )
    ]


encodeType : CreatureType -> Enc.Value
encodeType =
    customTypeEncoder typeMapping


typeDecoder : Decoder CreatureType
typeDecoder =
    customTypeDecoder typeMapping


encodeMovement : MovementType -> Enc.Value
encodeMovement movement =
    let
        ( moveType, speed ) =
            case movement of
                Walk ft ->
                    ( "WALK", ft )

                Burrow ft ->
                    ( "BURROW", ft )

                Climb ft ->
                    ( "CLIMB", ft )

                Fly ft ->
                    ( "FLY", ft )

                Swim ft ->
                    ( "SWIM", ft )
    in
    Enc.list identity
        [ Enc.string moveType
        , Enc.int speed
        ]


encodeSense : Sense -> Enc.Value
encodeSense sense =
    case sense of
        Blindsight ft ->
            Enc.list identity [ Enc.string "BLINDSIGHT", Enc.int ft ]

        Darkvision ft ->
            Enc.list identity [ Enc.string "DARKVISION", Enc.int ft ]

        Tremorsense ->
            Enc.string "TREMORSENSE"

        Truesight ->
            Enc.string "TRUESIGHT"


encodeAction : Action -> Enc.Value
encodeAction action =
    Enc.object
        [ ( "name", Enc.string action.name )
        , ( "description", Enc.string action.description )
        , ( "charges"
          , case action.charges of
                Nothing ->
                    Enc.null

                Just count ->
                    Enc.int count
          )
        ]


decoder : Decode.Decoder Creature
decoder =
    Decode.succeed Creature
        |> required "id" Decode.string
        |> required "name" Decode.string
        |> required "size" sizeDecoder
        |> required "type" typeDecoder
        |> required "typeTags" (Decode.list Decode.string)
        |> required "alignment" Alignment.decoder
        |> required "ac" Decode.int
        |> required "movement" (Decode.list movementDecoder)
        |> required "hp" Decode.int
        |> required "abilityScores" Abilities.decoder
        -- |> required "senses" (Decode.list senseDecoder)
        |> required "xp" Decode.int



--        |> required "attacks" (Decode.list Attack.decoder)
--        |> required "actions" (Decode.list actionDecoder)


movementDecoder : Decoder MovementType
movementDecoder =
    union1Decoder
        [ ( "WALK", ( Walk, Decode.int ) )
        , ( "BURROW", ( Burrow, Decode.int ) )
        , ( "CLIMB", ( Climb, Decode.int ) )
        , ( "FLY", ( Fly, Decode.int ) )
        , ( "SWIM", ( Swim, Decode.int ) )
        ]


senseDecoder : Decoder Sense
senseDecoder =
    Decode.oneOf
        [ union1Decoder
            [ ( "BLINDSIGHT", ( Blindsight, Decode.int ) )
            , ( "DARKVISION", ( Darkvision, Decode.int ) )
            ]
        , customTypeDecoder
            [ ( "TREMORSENSE", Tremorsense )
            , ( "TRUESIGHT", Truesight )
            ]
        ]


actionDecoder : Decoder Action
actionDecoder =
    Decode.succeed Action
        |> required "name" Decode.string
        |> required "charges" (Decode.nullable Decode.int)
        |> required "description" Decode.string
