module Data.Helpers exposing (customTypeDecoder, customTypeEncoder, union1Decoder)

import AllDict
import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode


customTypeEncoder : List ( String, a ) -> (a -> Encode.Value)
customTypeEncoder mappingList =
    let
        mappings =
            List.map (\( str, val ) -> ( val, str )) mappingList
                |> AllDict.fromList
    in
    \val ->
        case AllDict.get val mappings of
            Just str ->
                Encode.string str

            Nothing ->
                Encode.string "ENCODER FAILURE"


customTypeDecoder : List ( String, a ) -> Decoder a
customTypeDecoder mappingList =
    let
        mappings =
            Dict.fromList mappingList
    in
    Decode.string
        |> Decode.andThen
            (\typeName ->
                case Dict.get typeName mappings of
                    Just result ->
                        Decode.succeed result

                    Nothing ->
                        Decode.fail (failureMsg typeName mappings)
            )


union1Decoder : List ( String, ( a -> b, Decoder a ) ) -> Decoder b
union1Decoder constructors =
    let
        mappings =
            Dict.fromList constructors
    in
    Decode.index 0 Decode.string
        |> Decode.andThen
            (\typeName ->
                case Dict.get typeName mappings of
                    Just ( con, decoder ) ->
                        Decode.map con (Decode.index 1 decoder)

                    Nothing ->
                        Decode.fail (failureMsg typeName mappings)
            )


failureMsg : String -> Dict String a -> String
failureMsg provided mappings =
    "Value "
        ++ provided
        ++ " not recognized. Valid values are: "
        ++ String.join ", " (Dict.keys mappings)
