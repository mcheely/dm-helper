module Data.Abilities exposing (AbilityScores, decoder, statBonus, toJson)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Enc


type alias AbilityScores =
    { str : Int
    , dex : Int
    , con : Int
    , int : Int
    , wis : Int
    , cha : Int
    }


statBonus : Int -> Int
statBonus statValue =
    (statValue // 2) - 5



-- Encode/Decode


toJson : AbilityScores -> Enc.Value
toJson abilities =
    Enc.object
        [ ( "strength", Enc.int abilities.str )
        , ( "dexterity", Enc.int abilities.dex )
        , ( "constitution", Enc.int abilities.con )
        , ( "intelligence", Enc.int abilities.int )
        , ( "wisdom", Enc.int abilities.wis )
        , ( "charisma", Enc.int abilities.cha )
        ]


decoder : Decoder AbilityScores
decoder =
    Decode.succeed AbilityScores
        |> required "strength" Decode.int
        |> required "dexterity" Decode.int
        |> required "constitution" Decode.int
        |> required "intelligence" Decode.int
        |> required "wisdom" Decode.int
        |> required "charisma" Decode.int
