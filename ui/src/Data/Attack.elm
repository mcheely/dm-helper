module Data.Attack exposing (Attack, Damage, DamageType(..), Dice, Range(..), decoder, toJson)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Enc


type alias Dice =
    { quantity : Int
    , sides : Int
    }


type DamageType
    = Bludgeoning
    | Piercing
    | Slashing
    | Poison


type alias Damage =
    { dice : Dice
    , bonus : Int
    , dmgType : DamageType
    }


type Range
    = Melee Int
    | Ranged Int Int


type alias Attack =
    { name : String
    , range : Range
    , hitBonus : Int
    , damage : List Damage
    }



-- Encode/Decode


toJson : Attack -> Enc.Value
toJson attack =
    Enc.object
        [ ( "name", Enc.string attack.name )
        , ( "range", rangeJson attack.range )
        , ( "hitBonus", Enc.int attack.hitBonus )
        , ( "damage", Enc.list damageJson attack.damage )
        ]


rangeJson : Range -> Enc.Value
rangeJson range =
    case range of
        Melee distance ->
            Enc.int distance

        Ranged normal long ->
            Enc.list Enc.int [ normal, long ]


damageJson : Damage -> Enc.Value
damageJson damage =
    Enc.object
        [ ( "dice", diceJson damage.dice )
        , ( "bonus", Enc.int damage.bonus )
        , ( "type", dmgTypeJson damage.dmgType )
        ]


dmgTypeJson : DamageType -> Enc.Value
dmgTypeJson dmgType =
    Enc.string <|
        case dmgType of
            Bludgeoning ->
                "BLUDGEONING"

            Piercing ->
                "PIERCING"

            Slashing ->
                "SLASHING"

            Poison ->
                "POISON"


diceJson : Dice -> Enc.Value
diceJson dice =
    Enc.object
        [ ( "quantity", Enc.int dice.quantity )
        , ( "sides", Enc.int dice.sides )
        ]


decoder : Decoder Attack
decoder =
    Decode.succeed Attack
        |> required "name" Decode.string
        |> required "range" rangeDecoder
        |> required "hitBonus" Decode.int
        |> required "damage" (Decode.list damageDecoder)


rangeDecoder : Decoder Range
rangeDecoder =
    Decode.oneOf
        [ Decode.map Melee Decode.int
        , Decode.map2 Ranged
            (Decode.index 0 Decode.int)
            (Decode.index 1 Decode.int)
        ]


damageDecoder : Decoder Damage
damageDecoder =
    Decode.succeed Damage
        |> required "dice" diceDecoder
        |> required "bonus" Decode.int
        |> required "type" dmgTypeDecoder


dmgTypeDecoder : Decoder DamageType
dmgTypeDecoder =
    Decode.string
        |> Decode.andThen
            (\str ->
                case str of
                    "BLUDGEONING" ->
                        Decode.succeed Bludgeoning

                    "PIERCING" ->
                        Decode.succeed Piercing

                    "SLASHING" ->
                        Decode.succeed Slashing

                    "POISON" ->
                        Decode.succeed Poison

                    _ ->
                        Decode.fail ("Unrecognized damage type: " ++ str)
            )


diceDecoder : Decoder Dice
diceDecoder =
    Decode.succeed Dice
        |> required "quantity" Decode.int
        |> required "sides" Decode.int
