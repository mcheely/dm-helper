module Main exposing (main)

import Api
import Browser
import Browser.Navigation as Nav
import Data.Creature as Creature exposing (Creature)
import Html exposing (Html, pre, text)
import Html.Attributes exposing (..)
import Http exposing (Error(..))
import Url



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , creatureResponse : Maybe (Result Http.Error Creature)
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    ( { key = key
      , url = url
      , creatureResponse = Nothing
      }
    , Api.getCreature CreatureResponse "goblin"
    )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | CreatureResponse (Result Http.Error Creature)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )

        CreatureResponse response ->
            ( { model | creatureResponse = Just response }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "DM Helper"
    , body =
        [ case model.creatureResponse of
            Nothing ->
                text "Loading..."

            Just response ->
                showResponse response
        ]
    }


showResponse : Result Http.Error Creature -> Html msg
showResponse result =
    case result of
        Err error ->
            case error of
                BadPayload msg whatev ->
                    pre [] [ text msg ]

                _ ->
                    text "Error!"

        Ok creature ->
            text "Success!"
