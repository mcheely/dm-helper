module Api exposing (getCreature)

import Data.Creature as Creature
import GraphQl as Query exposing (query)
import GraphQl.Http as Http
import Json.Decode as Decode


creatureNameQuery name =
    Query.object
        [ Query.field "creatureByName"
            |> Query.withArgument "name" (Query.string name)
            |> Query.withSelectors
                [ Query.field "name"
                , Query.field "armorClass"
                , Query.field "hitDiceCount"
                , Query.field "hitDiceSides"
                , Query.field "walkSpeed"
                , Query.field "climbSpeed"
                , Query.field "flySpeed"
                , Query.field "xp"
                , Query.field "creatureType"
                , Query.field "abilityScores"
                    |> Query.withSelectors
                        [ Query.field "strength"
                        , Query.field "dexterity"
                        , Query.field "constitution"
                        , Query.field "intelligence"
                        , Query.field "wisdom"
                        , Query.field "charisma"
                        ]
                ]
        ]


getCreature msg name =
    creatureNameQuery name
        |> query
        |> Http.send "http://localhost:5000/graphql"
            msg
            (Decode.field "creatureByName" Creature.decoder)
