module Example exposing (suite)

import Data.Creature as Creature exposing (..)
import Expect exposing (Expectation)
import Fuzzers exposing (creature)
import Json.Decode as Decode
import Json.Encode as Encode
import Result
import Test exposing (..)


suite : Test
suite =
    describe "The Creature Type"
        [ fuzz creature "Can be serialized and deseralized to json" <|
            \creature ->
                creature
                    |> Creature.encode
                    |> Encode.encode 0
                    |> Decode.decodeString Creature.decoder
                    |> Expect.equal (Result.Ok creature)
        ]
