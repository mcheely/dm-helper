module Fuzzers exposing (abilityScores, action, alignment, attack, creature, creatureType, damage, damageType, dice, movementType, range, sense, size)

import Data.Abilities exposing (..)
import Data.Alignment exposing (..)
import Data.Attack exposing (..)
import Data.Creature as Creature exposing (..)
import Fuzz
    exposing
        ( Fuzzer
        , andMap
        , constant
        , int
        , list
        , map
        , map2
        , map3
        , map4
        , maybe
        , oneOf
        , string
        , tuple
        )


creature : Fuzzer Creature
creature =
    Fuzz.map Creature string
        |> andMap string
        |> andMap size
        |> andMap creatureType
        |> andMap (list string)
        |> andMap alignment
        |> andMap int
        |> andMap (list movementType)
        |> andMap int
        |> andMap abilityScores
        |> andMap (list sense)
        |> andMap int
        |> andMap (list attack)
        |> andMap (list action)


size : Fuzzer Size
size =
    oneOf
        [ constant Tiny
        , constant Small
        , constant Medium
        , constant Large
        , constant Huge
        , constant Gargantuan
        ]


creatureType : Fuzzer CreatureType
creatureType =
    oneOf
        [ constant Abberation
        , constant Beast
        , constant Celestial
        , constant Construct
        , constant Dragon
        , constant Elemental
        , constant Fey
        , constant Fiend
        , constant Giant
        , constant Humanoid
        , constant Monstrosity
        , constant Ooze
        , constant Plant
        , constant Undead
        ]


alignment : Fuzzer Alignment
alignment =
    oneOf
        [ constant Unaligned
        , map Aligned <|
            tuple
                ( oneOf
                    [ constant Lawful
                    , constant LawNeutral
                    , constant Chaotic
                    ]
                , oneOf
                    [ constant Good
                    , constant MoralNeutral
                    , constant Evil
                    ]
                )
        ]


movementType : Fuzzer MovementType
movementType =
    oneOf
        [ map Walk int
        , map Burrow int
        , map Climb int
        , map Fly int
        , map Swim int
        ]


abilityScores : Fuzzer AbilityScores
abilityScores =
    map AbilityScores int
        |> andMap int
        |> andMap int
        |> andMap int
        |> andMap int
        |> andMap int


sense : Fuzzer Sense
sense =
    oneOf
        [ map Blindsight int
        , map Darkvision int
        , constant Tremorsense
        , constant Truesight
        ]


attack : Fuzzer Attack
attack =
    map4 Attack
        string
        range
        int
        (list damage)


range : Fuzzer Range
range =
    oneOf
        [ map Melee int
        , map2 Ranged int int
        ]


damage : Fuzzer Damage
damage =
    map3 Damage
        dice
        int
        damageType


dice : Fuzzer Dice
dice =
    map2 Dice int int


damageType : Fuzzer DamageType
damageType =
    oneOf
        [ constant Bludgeoning
        , constant Piercing
        , constant Slashing
        , constant Poison
        ]


action : Fuzzer Action
action =
    map3 Action
        string
        (maybe int)
        string
