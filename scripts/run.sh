#! /usr/bin/env bash

postgraphile \
    -c postgres:///dm_helper \
    -s app_public \
    --cors \
    --append-plugins @graphile-contrib/pg-simplify-inflector
