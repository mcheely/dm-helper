-- migrate:up
create extension pgcrypto;
create schema app_public;


-- migrate:down
drop schema app_public;
drop extension pgcrypto;

