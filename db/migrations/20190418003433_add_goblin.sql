-- migrate:up

with stat as (
  insert into app_public.ability_scores
    (strength, dexterity, constitution, intelligence, wisdom, charisma) values
    (8, 14, 10, 10, 8, 8)
    returning id
)
insert into app_public.creatures
  (name, ability_scores_id, armor_class, hit_dice_count, hit_dice_sides, walk_speed, creature_type, xp) values
  ('goblin', (select id from stat), 15, 2, 6, 30, 'HUMANOID', 50);

-- migrate:down

delete from app_public.creatures where name in ('goblin');

