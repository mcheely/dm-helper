-- migrate:up
create table app_public.ability_scores(
  id uuid primary key default gen_random_uuid(),
  strength smallint not null,
  dexterity smallint not null,
  constitution smallint not null,
  intelligence smallint not null,
  wisdom smallint not null,
  charisma smallint not null
);

create table app_public.creatures(
  id uuid primary key default gen_random_uuid(),
  name varchar(120) unique not null,
  ability_scores_id uuid references app_public.ability_scores on delete cascade not null,
  armor_class smallint not null,
  hit_dice_count smallint not null,
  hit_dice_sides smallint not null,
  walk_speed smallint not null,
  climb_speed smallint not null default 0,
  fly_speed smallint not null default 0,
  xp int not null,
  creature_type varchar(50) not null
);

create unique index creature_name on app_public.creatures ((lower(name)));

-- migrate:down
drop table if exists app_public.creatures;
drop table if exists app_public.ability_scores;
drop index if exists creature_name;
