SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: app_public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA app_public;


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ability_scores; Type: TABLE; Schema: app_public; Owner: -
--

CREATE TABLE app_public.ability_scores (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    strength smallint NOT NULL,
    dexterity smallint NOT NULL,
    constitution smallint NOT NULL,
    intelligence smallint NOT NULL,
    wisdom smallint NOT NULL,
    charisma smallint NOT NULL
);


--
-- Name: creatures; Type: TABLE; Schema: app_public; Owner: -
--

CREATE TABLE app_public.creatures (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name character varying(120) NOT NULL,
    ability_scores_id uuid NOT NULL,
    armor_class smallint NOT NULL,
    hit_dice_count smallint NOT NULL,
    hit_dice_sides smallint NOT NULL,
    walk_speed smallint NOT NULL,
    climb_speed smallint DEFAULT 0 NOT NULL,
    fly_speed smallint DEFAULT 0 NOT NULL,
    xp integer NOT NULL,
    creature_type character varying(50) NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: ability_scores ability_scores_pkey; Type: CONSTRAINT; Schema: app_public; Owner: -
--

ALTER TABLE ONLY app_public.ability_scores
    ADD CONSTRAINT ability_scores_pkey PRIMARY KEY (id);


--
-- Name: creatures creatures_name_key; Type: CONSTRAINT; Schema: app_public; Owner: -
--

ALTER TABLE ONLY app_public.creatures
    ADD CONSTRAINT creatures_name_key UNIQUE (name);


--
-- Name: creatures creatures_pkey; Type: CONSTRAINT; Schema: app_public; Owner: -
--

ALTER TABLE ONLY app_public.creatures
    ADD CONSTRAINT creatures_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: creature_name; Type: INDEX; Schema: app_public; Owner: -
--

CREATE UNIQUE INDEX creature_name ON app_public.creatures USING btree (lower((name)::text));


--
-- Name: creatures creatures_ability_scores_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: -
--

ALTER TABLE ONLY app_public.creatures
    ADD CONSTRAINT creatures_ability_scores_id_fkey FOREIGN KEY (ability_scores_id) REFERENCES app_public.ability_scores(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20190417225037'),
    ('20190417225240'),
    ('20190418003433');
